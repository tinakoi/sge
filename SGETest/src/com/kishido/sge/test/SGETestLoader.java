package com.kishido.sge.test;

import android.os.Bundle;

import com.kishido.sge.GameLoader;

public class SGETestLoader extends GameLoader {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setup(new FPSTest());
	}
}
