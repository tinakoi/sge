package com.kishido.sge.test;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.kishido.sge.Game;

public class FPSTest extends Game {

	private Paint paint;

	private long timeElapsed = 0;
	private int updateCount = 0;
	
	@Override
	public void initResources() {
		this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.paint.setColor(Color.WHITE);
		this.paint.setTextSize(50);
	}

	@Override
	public void update(long elapsedTime) {
		if (this.updateCount < 1000) {
			this.updateCount++;
			this.timeElapsed += elapsedTime;
		}
	}

	@Override
	public void render(Canvas canvas) {
		// render black background
		canvas.drawColor(Color.BLACK);

		// render fps
		canvas.drawText("FPS: " + this.getCurrentFPS(), 0, this.getScreenHeight(), paint);
		canvas.drawText("UPDATE: " + this.updateCount, 0, this.getScreenHeight() - 50, paint);
		canvas.drawText("TIME: " + this.timeElapsed, 0, this.getScreenHeight() - 100, paint);
	}

}
