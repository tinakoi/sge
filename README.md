**Simple Game Engine (SGE)**

This is a basic framework for developing games in Android. As of now, it only includes the basic game loop implementation along with only the SurfaceView graphics rendering support.

**Simple to Use**

To use this library, just follow the steps below:

1. Override GameLoader class and put it in your AndroidManifest file as your main activity. Make sure to add the setup(Game game) at the onCreate method
```
#!java
TestGameLoader extends GameLoader {

     onCreate(Bundle b) {
          super.onCreate(b);
          setup(new TestGame());
     }
}

```

2. Override Game class and put all your logic code here. Read the documentation for more details.