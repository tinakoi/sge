package com.kishido.sge;

import android.content.pm.ActivityInfo;

public class GameConfig {

	/**
	 * If this flag is true, then the game will be rendered in fullscreen. This
	 * means that the status bar at the top of an Android device will not be
	 * displayed anymore.
	 * 
	 * By default, a game will run in fullscreen. Change this value to false if
	 * you don't want to run in fullscreen mode.
	 */
	public boolean fullscreen = true;

	/**
	 * If this flag is true, then the game name will be displayed in the action
	 * bar at the top of the screen just below the status bar.
	 * 
	 * By default, a game will not display the title. Change this value to false
	 * if you want to display the title.
	 */
	public boolean showTitle = false;

	/**
	 * Set the screen orientation of the game. Currently, there are only 3
	 * supported screen orientation configuration which are the following:
	 * Portrait, Landscape and Unspecified.
	 * 
	 * By default, the game is locked to Portrait orientation. Change this value
	 * to either Unspecified or Landscape for your own preferences.
	 */
	public int screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

	/**
	 * Set the standard width and height to be used for graphics scaling
	 * throughout the game (e.g. image loading and rendering).
	 * 
	 * By default, the standard dimension is 480 x 640. Change the value of each
	 * to your own preferences.
	 */
	public int standardWidth = 480;
	public int standardHeight = 640;
}
