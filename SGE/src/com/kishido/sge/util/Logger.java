package com.kishido.sge.util;

import android.util.Log;

public class Logger {

    public static boolean DEBUG_MODE = true;
    
    public static void info(String message) {
        if (DEBUG_MODE) {
            Log.i(Logger.class.getName(), message);
        }
    }
    
    public static void warn(String message) {
        if (DEBUG_MODE) {
            Log.w(Logger.class.getName(), message);
        }
    }

    public static void error(String message) {
        if (DEBUG_MODE) {
            Log.e(Logger.class.getName(), message);
        }
    }

    public static void debug(String message) {
        if (DEBUG_MODE) {
            Log.d(Logger.class.getName(), message);
        }
    }
}
