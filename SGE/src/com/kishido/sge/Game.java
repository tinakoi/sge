package com.kishido.sge;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.view.Window;
import android.view.WindowManager;

import com.kishido.sge.engine.Graphics;
import com.kishido.sge.engine.SystemTimer;

public abstract class Game implements Runnable {

    private Thread thread;

    private Graphics graphics;
    private SystemTimer timer;

    private boolean initialized;
    private boolean running;
    private boolean inFocus;
    
    @Override
    public void run() {
    	System.gc();
        System.runFinalization();
        
        this.loadResources();
        
        this.timer.start();
        this.timer.refresh();
        
        long elapsedTime = 0;
        
        while (this.running) {
            if (this.inFocus) {
                this.update(elapsedTime);
            }
            
            Canvas canvas = this.graphics.getCanvas();
            if (canvas != null) {
                this.render(canvas);
                this.graphics.postCanvas(canvas);
            }
            
            elapsedTime = this.timer.sleep();
            
            if (elapsedTime > 100) {
                elapsedTime = 100;
            }
        }
        
        this.timer.stop();
        this.graphics.reset();
    }

    public abstract void initResources();
    
    public abstract void update(long elapsedTime);
    
    public abstract void render(Canvas canvas);
    
    public void start() {
        if (this.running) {
            return;
        }
        
        this.running = true;
        
        this.thread = new Thread(this);
        this.thread.start();
    }
    
    public void stop() {
        if (!this.running) {
            return;
        }
        
        this.running = false;
        
        boolean retry = true;
        
        while (retry) {
            try {
                this.thread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void pause() {
        this.inFocus = false;
    }
    
    public void resume() {
        this.inFocus = true;
    }
    
    public void setup(Activity activity, GameConfig config) {
        this.initializeEngine(activity);

        this.loadConfiguration(activity, config);
        activity.setContentView(this.graphics.getSurface());
    }
    
    private void loadConfiguration(Activity activity, GameConfig config) {
        // check if screen orientation config is supported
        if (config.screenOrientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                && config.screenOrientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                && config.screenOrientation != ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
            throw new RuntimeException("Unsupported screen orientation.");
        }
        
        if (!config.showTitle) {
            activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        
        if (config.fullscreen) {
            activity.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            );
        }
        
        activity.setRequestedOrientation(config.screenOrientation);
        
        this.graphics.setStandardDimension(config.standardWidth, config.standardHeight);
    }

	private void initializeEngine(Context context) {
    	if (this.graphics == null) {
            this.graphics = new Graphics(context);
        }
        
        if (this.timer == null) {
            this.timer = new SystemTimer();
        }
    }

    // try to load resources and initialize game objects.
    // some game objects might require values for the
    // screen width and height which are initialized in
    // the graphics engine.
    private synchronized void loadResources() {
        while (!this.initialized) {
            if (this.graphics.isReady()) {
                this.initResources();
                this.initialized = true;
            }
        }
    }
    
    public int getScreenWidth() {
        return this.graphics.getScreenWidth();
    }
    
    public int getScreenHeight() {
        return this.graphics.getScreenHeight();
    }
    
    public int getCurrentFPS() {
        return this.timer.getCurrentFPS();
    }

    public float getScale() {
    	return this.graphics.getMinScale();
    }
    
}
