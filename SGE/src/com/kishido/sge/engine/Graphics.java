package com.kishido.sge.engine;

import android.content.Context;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class Graphics implements Callback {

    private static int STANDARD_WIDTH;
    private static int STANDARD_HEIGHT;
    
    private SurfaceView surface;
    private SurfaceHolder holder;
    
    private boolean ready;
    
    private int screenWidth;
    private int screenHeight;
    
    private float scaleWidth;
    private float scaleHeight;
    
    public Graphics(Context context) {
        this.surface = new SurfaceView(context);
        this.holder = this.surface.getHolder();
        this.holder.addCallback(this);
    }
    
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
        this.ready = true;
        
        this.screenWidth = width;
        this.screenHeight = height;
        
        this.scaleWidth = (float) width / STANDARD_WIDTH;
        this.scaleHeight = (float) height / STANDARD_HEIGHT;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        this.ready = false;
    }

    public boolean isReady() {
        return this.ready;
    }
    
    public int getScreenWidth() {
        return this.screenWidth;
    }
    
    public int getScreenHeight() {
        return this.screenHeight;
    }
    
    public Canvas getCanvas() {
        return this.holder.lockCanvas();
    }
    
    public float getScaleWidth() {
        return this.scaleWidth;
    }
    
    public float getScaleHeight() {
        return this.scaleHeight;
    }
    
    public float getMaxScale() {
        return Math.max(this.scaleWidth, this.scaleHeight);
    }
    
    public float getMinScale() {
        return Math.min(this.scaleWidth, this.scaleHeight);
    }
    
    public void postCanvas(Canvas canvas) {
        this.holder.unlockCanvasAndPost(canvas);
    }
    
    public SurfaceView getSurface() {
        return this.surface;
    }
    
    public void reset() {
        this.ready = false;
        this.screenWidth = this.screenHeight = 0;
    }
    
    public void setStandardDimension(int width, int height) {
        Graphics.STANDARD_WIDTH = width;
        Graphics.STANDARD_HEIGHT = height;
    }
}
