package com.kishido.sge.engine;

public class SystemTimer {

    private int fps = 50;
    private long delay;

    private long start;
    private long end;
    private long sleepTime;

    private long overSleepTime; // hold drift on Thread.sleep() method

    private boolean running;
    private FPSCounter fpsCounter;

    public SystemTimer() {
        this.fpsCounter = new FPSCounter();
    }

    public int getCurrentFPS() {
        return this.fpsCounter.getCurrentFPS();
    }

    public int getFPS() {
        return this.fps;
    }

    public long getTime() {
        return System.currentTimeMillis();
    }

    public boolean isRunning() {
        return this.running;
    }

    public void refresh() {
        this.start = System.currentTimeMillis();
        this.overSleepTime = 0;
    }

    public void setFPS(int fps) {
        if (this.fps == fps) {
            return;
        }
        this.fps = fps;

        if (this.running) {
            this.start();
        }
    }

    public long sleep() {
        this.end = System.currentTimeMillis();

        long diff = this.end - this.start;
        this.sleepTime = this.delay - diff - this.overSleepTime;

        if (this.sleepTime > 0) {
            
            try {
                Thread.sleep(this.sleepTime);
            } catch (InterruptedException e) {
                
            }

            this.overSleepTime = System.currentTimeMillis() - this.end
                    - this.sleepTime;

        } else {
            
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                
            }

            this.overSleepTime = 0;
        }

        this.fpsCounter.calculateFPS();

        long end = System.currentTimeMillis();
        long elapsedTime = end - this.start;
        this.start = end;

        return elapsedTime;
    }

    public void start() {
        if (this.running) {
            this.stop();
        }
        this.running = true;
        this.delay = 1000 / this.fps;
        this.refresh();
        this.fpsCounter.refresh();
    }

    public void stop() {
        this.running = false;
    }

    public class FPSCounter {

        private long lastCount;

        private int currentFPS;
        private int frameCount;

        public FPSCounter() {
        }

        public void calculateFPS() {
            this.frameCount++;
            if (System.currentTimeMillis() - this.lastCount > 1000) {
                this.lastCount = System.currentTimeMillis();
                this.currentFPS = this.frameCount;
                this.frameCount = 0;
            }
        }

        public int getCurrentFPS() {
            return this.currentFPS;
        }

        public void refresh() {
            this.frameCount = 0;
            this.lastCount = System.currentTimeMillis();
        }
    }
}
