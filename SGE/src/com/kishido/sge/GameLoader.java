package com.kishido.sge;

import android.app.Activity;

public class GameLoader extends Activity {

    private Game game;
    
    public void setup(Game game) {
        this.setup(game, new GameConfig());
    }
    
    public void setup(Game game, GameConfig config) {
        this.game = game;
        this.game.setup(this, config);
    }
    
    @Override
    protected void onStart() {
        super.onStart();

        this.game.start();
    }
    
    @Override
    protected void onResume() {
        super.onResume();

        this.game.resume();
    }
    
    @Override
    protected void onPause() {
        super.onPause();

        this.game.pause();
    }
    
    @Override
    protected void onStop() {
        super.onStop();

        this.game.stop();
    }
}
